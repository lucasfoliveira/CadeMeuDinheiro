import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { StatusBar } from '@ionic-native/status-bar';
import { Financa } from '../../models/financa';
import { FinancaServiceProvider } from '../../providers/financa-service/financa-service';
import { MetaServiceProvider } from '../../providers/meta-service/meta-service';
import { Amizade } from '../../models/amizade';
import { AmigosServiceProvider } from '../../providers/amigos-service/amigos-service';
import { Verificacao } from '../../models/VerificacaoEnum';
import { AmigosPageModule } from '../amigos/amigos.module';


@IonicPage()
@Component({
  selector: 'page-adiciona-financa',
  templateUrl: 'adiciona-financa.html',
})
export class AdicionaFinancaPage {

  public financa = {} as Financa;
  public valorFinanca;
  public tipoFinanca = "";
  public financaCompartilhada = false;
  public usuarioCompartilhado = "";
  public usuarioCompartilhado2 = "";
  public usuarioCompartilhado3 = "";
  public usuarioCompartilhado4 = "";
  public usuarioCompartilhado5 = "";
  public usuarioCompartilhado6 = "";
  public usuarioCompartilhado7 = "";
  public usuarioCompartilhado8 = "";
  public usuarioCompartilhado9 = "";
  public liberaUsuario2 = false;
  public liberaUsuario3 = false;
  public liberaUsuario4 = false;
  public liberaUsuario5 = false;
  public liberaUsuario6 = false;
  public liberaUsuario7 = false;
  public liberaUsuario8 = false;
  public liberaUsuario9 = false;
  public numeroUsuariosCompartilhados = 0;
  public listaDeAmigos: Amizade[] = [];
  public listaCompartilhada: String[] = [];

  //booleans de verificação
  valorInvalido = false;
  valorVazio = false;
  descricaoVazia = false;
  dataVazia = false;
  categoriaVazia = false;
  tipoVazio = false;
  usuarioCompartilhadoVazio = false;
  usuarioCompartilhadoVazio2 = false;
  usuarioCompartilhadoVazio3 = false;
  usuarioCompartilhadoVazio4 = false;
  usuarioCompartilhadoVazio5 = false;
  usuarioCompartilhadoVazio6 = false;
  usuarioCompartilhadoVazio7 = false;
  usuarioCompartilhadoVazio8 = false;
  usuarioCompartilhadoVazio9 = false;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCtrl: ToastController, 
    public statusBar: StatusBar,
    public authService: AuthProvider,
    public alertCtrl: AlertController,
    public financaService: FinancaServiceProvider,
    public metaService: MetaServiceProvider,
    public amigosService: AmigosServiceProvider) {
  }

  private abrirToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'bottom'
    });
    toast.present();
  }

  public adicionaFinanca() {
    var usuariosOk = false;
    // TODO: remover depois de adicionar os campos
    // categoria e ehDebito ao form de criação de financas
    if (!this.financaCompartilhada) {
      this.adicionaFinancaAux();
    }
    else {
      if (!this.confirmaDados()) {
        this.abrirToast("Ops! Parece que você deixou de preencher algo, ou preencheu incorretamente.");
      }
      else {
      if (this.tipoFinanca === "debito") {
        this.financa = {
          ...this.financa,
          valor: this.retornaValorPorUsuario(),
          ehDebito: true
        };
      }
      else {
        this.financa = {
          ...this.financa,
          valor: this.retornaValorPorUsuario(),
          ehDebito: false
        };
      }
      // No caso, precisa criar um adicionaFinancaEmUsuarioFB para adicionar o objeto financa novo (com valor dividido por usuários) em cada usuário da finança

      this.carregaAmigos().then(listaDeAmigos => {
        this.listaCompartilhada.forEach(username => {
          this.financaService.adicionaFinancaEmUsuarioFB(username as string, this.financa);
          this.financaService.adicionaFinancaFB(this.financa)
          .then(_ => {
            this.navCtrl.pop();
            usuariosOk = true;
          });
        });
      });
      if (!usuariosOk) {
        this.abrirToast("Ops! Parece que um ou mais usuários que você registrou na finança não está na sua lista de amizades.");
      }
    }
  }
}

  public adicionaFinancaAux() {
    if (!this.confirmaDados()) {
      this.abrirToast("Ops! Parece que você deixou de preencher algo, ou preencheu incorretamente.");
    } else {
      if (this.tipoFinanca === "debito") {
        this.financa = {
          ...this.financa,
          valor: this.valorFinanca,
          ehDebito: true
        };
      } else {
        this.financa = {
          ...this.financa,
          valor: this.valorFinanca,
          ehDebito: false
        };
      }
      this.financaService.adicionaFinancaFB(this.financa)
      .then(_ => {
        this.metaService.contabilizaFinanca(this.financa);
        this.navCtrl.pop();
      });
    }
  }

  public updateFinancaCompartilhada() {
    if (!this.financaCompartilhada) {
      this.financaCompartilhada = true;
    }
    else {
      this.financaCompartilhada = false;
    }
  }

  public retornaValorPorUsuario() {
    var valor = 0.0;
    this.numeroUsuariosCompartilhados = this.getNumeroUsuariosCompartilhados();
    valor = parseFloat(this.valorFinanca) / this.numeroUsuariosCompartilhados;
    return valor;
  }

  public getNumeroUsuariosCompartilhados() {
    if (this.liberaUsuario9) {
      return 10;
    }
    else if (this.liberaUsuario8) {
      return 9;
    }
    else if (this.liberaUsuario7) {
      return 8;
    }
    else if (this.liberaUsuario6) {
      return 7;
    }
    else if (this.liberaUsuario5) {
      return 6;
    }
    else if (this.liberaUsuario4) {
      return 5;
    }
    else if (this.liberaUsuario3) {
      return 4;
    }
    else if (this.liberaUsuario2) {
      return 3;
    }
    else {
      return 2;
    }
  }

  public usuarioEstaNaListaDeAmigos(usuario) {
    if(this.financaCompartilhada){
      this.carregaAmigos();
      for(var i = 0; i < this.listaDeAmigos.length; i+1){
        if(usuario === this.listaDeAmigos[i].amigo){
          return true;
        }
      }
      return false;
    }
  }

  public adicionaOutroUsuario() {
    if(this.liberaUsuario9) {
      this.abrirToast("Ops! Você não pode adicionar finança com mais de 10 usuários.");
    }
    else if(this.liberaUsuario8){
      this.liberaUsuario9 = true;
    }
    else if(this.liberaUsuario7){
      this.liberaUsuario8 = true;
    }
    else if(this.liberaUsuario6){
      this.liberaUsuario7 = true;
    }
    else if(this.liberaUsuario5){
      this.liberaUsuario6 = true;
    }
    else if(this.liberaUsuario4){
      this.liberaUsuario5 = true;
    }
    else if(this.liberaUsuario3){
      this.liberaUsuario4 = true;
    }
    else if(this.liberaUsuario2){
      this.liberaUsuario3 = true;
    }
    else{
      this.liberaUsuario2 = true;
    }
  }

private confirmaDados() {
  this.valorInvalido = this.financa.valor <= 0;
  this.valorVazio = this.valorFinanca == null;
  this.dataVazia = this.financa.data == null;
  this.categoriaVazia = this.financa.categoria == null;
  this.descricaoVazia = this.financa.descricao == "" || this.financa.descricao == null;
  this.tipoVazio = this.tipoFinanca == "";
  this.usuarioCompartilhadoVazio = this.usuarioCompartilhado == "";
  this.usuarioCompartilhadoVazio2 = this.usuarioCompartilhado2 == "" && this.liberaUsuario2;
  this.usuarioCompartilhadoVazio3 = this.usuarioCompartilhado3 == "" && this.liberaUsuario3;
  this.usuarioCompartilhadoVazio4 = this.usuarioCompartilhado4 == "" && this.liberaUsuario4;
  this.usuarioCompartilhadoVazio5 = this.usuarioCompartilhado5 == "" && this.liberaUsuario5;
  this.usuarioCompartilhadoVazio6 = this.usuarioCompartilhado6 == "" && this.liberaUsuario6;
  this.usuarioCompartilhadoVazio7 = this.usuarioCompartilhado7 == "" && this.liberaUsuario7;
  this.usuarioCompartilhadoVazio8 = this.usuarioCompartilhado8 == "" && this.liberaUsuario8;
  this.usuarioCompartilhadoVazio9 = this.usuarioCompartilhado9 == "" && this.liberaUsuario9;

  if (this.financaCompartilhada) {
    if (this.liberaUsuario9) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4
      && !this.usuarioCompartilhadoVazio5 && !this.usuarioCompartilhadoVazio6 && !this.usuarioCompartilhadoVazio7
      && !this.usuarioCompartilhadoVazio8 && !this.usuarioCompartilhadoVazio9;
    }
    else if (this.liberaUsuario8) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4
      && !this.usuarioCompartilhadoVazio5 && !this.usuarioCompartilhadoVazio6 && !this.usuarioCompartilhadoVazio7
      && !this.usuarioCompartilhadoVazio8;
    }
    else if (this.liberaUsuario7) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4
      && !this.usuarioCompartilhadoVazio5 && !this.usuarioCompartilhadoVazio6 && !this.usuarioCompartilhadoVazio7;
    }
    else if (this.liberaUsuario6) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4
      && !this.usuarioCompartilhadoVazio5 && !this.usuarioCompartilhadoVazio6;
    }
    else if (this.liberaUsuario5) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4
      && !this.usuarioCompartilhadoVazio5;
    }
    else if (this.liberaUsuario4) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3 && !this.usuarioCompartilhadoVazio4;
    }
    else if (this.liberaUsuario3) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2 && !this.usuarioCompartilhadoVazio3;
    }
    else if (this.liberaUsuario2) {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio
      && !this.usuarioCompartilhadoVazio2;
    }
    else {
      return !this.valorInvalido && !this.valorVazio &&
      !this.dataVazia && !this.descricaoVazia &&
      !this.categoriaVazia && !this.tipoVazio && !this.usuarioCompartilhadoVazio;
    }

  }
  else {
    return !this.valorInvalido && !this.valorVazio &&
           !this.dataVazia && !this.descricaoVazia &&
           !this.categoriaVazia && !this.tipoVazio;
  }
}

public carregaAmigos() {
  return new Promise((resolve) => {
    this.amigosService.recebeAmizadesFB()
    .then(amizades => {
      var allAmizades = amizades as Amizade[];
      allAmizades.forEach(amizade => {
        if (amizade.verificacao == Verificacao.Confirmado) {
          this.listaDeAmigos.push(amizade as Amizade);
          if (this.carregaAmigosAux(amizade.amigo)) {
            this.listaCompartilhada.push(amizade.amigo);
          }
        }
      });
      resolve(this.listaDeAmigos);
    });
  });

}

private carregaAmigosAux(username: string){
  return username === this.usuarioCompartilhado
      || username === this.usuarioCompartilhado2
      || username === this.usuarioCompartilhado3
      || username === this.usuarioCompartilhado4
      || username === this.usuarioCompartilhado5
      || username === this.usuarioCompartilhado6
      || username === this.usuarioCompartilhado7
      || username === this.usuarioCompartilhado8
      || username === this.usuarioCompartilhado9;
}

  public amigoDeTodosUsuarios() {
    if (this.liberaUsuario9) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado5) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado6)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado7) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado8)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado9));
    }
    else if (this.liberaUsuario8) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado5) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado6)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado7) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado8));
    }
    else if (this.liberaUsuario7) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado5) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado6)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado7));
    }
    else if (this.liberaUsuario6) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado5) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado6));
    }
    else if (this.liberaUsuario5) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado5));
    }
    else if (this.liberaUsuario4) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado4));
    }
    else if (this.liberaUsuario3) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2)
      && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado3));
    }
    else if (this.liberaUsuario2) {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado) && this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado2));
    }
    else {
      return (this.usuarioEstaNaListaDeAmigos(this.usuarioCompartilhado));
    }
  }

}
