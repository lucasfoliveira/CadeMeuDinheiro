import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, AlertController, IonicPageModule } from 'ionic-angular';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ChartsModule } from 'ng2-charts';

import { FIREBASE_CONFIG } from './firebase.credentials';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { OneSignal } from '@ionic-native/onesignal';
import { HttpClient, HttpClientModule, HttpRequest } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { DividaServiceProvider } from '../providers/divida-service/divida-service';
import { AcordoServiceProvider } from '../providers/acordo-service/acordo-service';
import { NotificationProvider } from '../providers/notification/notification';
import { AuthProvider } from '../providers/auth/auth';
import { FinancaServiceProvider } from '../providers/financa-service/financa-service';

import { MostraDividaPageModule } from '../pages/mostra-divida/mostra-divida.module';
import { MostraEmprestimoPageModule } from '../pages/mostra-emprestimo/mostra-emprestimo.module';
import { LoginCadastroPageModule } from '../pages/login-cadastro/login-cadastro.module';
import { EditaPerfilPageModule } from '../pages/edita-perfil/edita-perfil.module';
import { AdicionaAcordoPageModule } from '../pages/adiciona-acordo/adiciona-acordo.module';
import { EditaAcordoPageModule } from '../pages/edita-acordo/edita-acordo.module';
import { IntroducaoPageModule } from '../pages/introducao/introducao.module';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PerfilPage } from '../pages/perfil/perfil';
import { SobrePage } from '../pages/sobre/sobre';
import { FinancasPage } from '../pages/financas/financas';
import { FinancasChartsPage } from '../pages/financas-charts/financas-charts';

import { BrMaskerModule } from 'brmasker-ionic-3';
import { AdicionaEmprestimoPage } from '../pages/adiciona-emprestimo/adiciona-emprestimo';
import { AdicionaDividaPage } from '../pages/adiciona-divida/adiciona-divida';
import { AdicionaFinancaPage } from '../pages/adiciona-financa/adiciona-financa';
import { EditaDividaPage } from '../pages/edita-divida/edita-divida';
import { EditaEmprestimoPage } from '../pages/edita-emprestimo/edita-emprestimo';
import { EditaFinancaPage } from '../pages/edita-financa/edita-financa';
import { AdicionaMetaPage } from '../pages/adiciona-meta/adiciona-meta';
import { EditaMetaPage } from '../pages/edita-meta/edita-meta';
import { EditaMetaPageModule } from '../pages/edita-meta/edita-meta.module';
import { AdicionaMetaPageModule } from '../pages/adiciona-meta/adiciona-meta.module';
import { MetaServiceProvider } from '../providers/meta-service/meta-service';
import { AmigosServiceProvider } from '../providers/amigos-service/amigos-service';
import { AmigosPage } from '../pages/amigos/amigos';
import { AdicionaAmigoPage } from '../pages/adiciona-amigo/adiciona-amigo';
import { AtivosFinanceirosPage } from '../pages/ativos-financeiros/ativos-financeiros';
import { AtivosFinanceirosPageModule } from '../pages/ativos-financeiros/ativos-financeiros.module';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FinancasPage,
    PerfilPage,
    AmigosPage,
    SobrePage,
    AdicionaEmprestimoPage,
    AdicionaDividaPage,
    EditaDividaPage,
    EditaEmprestimoPage,
    AdicionaFinancaPage,
    EditaFinancaPage,
    FinancasChartsPage,
    AdicionaMetaPage,
    EditaMetaPage,
    AdicionaAmigoPage
    ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    IonicStorageModule.forRoot(),
    AngularFireDatabaseModule,
    MostraDividaPageModule,
    MostraEmprestimoPageModule,
    LoginCadastroPageModule,
    EditaPerfilPageModule,
    AngularFireAuthModule,
    AdicionaAcordoPageModule,
    EditaAcordoPageModule,
    IntroducaoPageModule,
    BrMaskerModule,
    ChartsModule,
    AtivosFinanceirosPageModule,
    IonicPageModule.forChild(AdicionaEmprestimoPage),
    IonicPageModule.forChild(AdicionaDividaPage),
    IonicPageModule.forChild(EditaEmprestimoPage),
    IonicPageModule.forChild(EditaDividaPage),
    IonicPageModule.forChild(AdicionaFinancaPage),
    IonicPageModule.forChild(EditaFinancaPage),
    IonicPageModule.forChild(AdicionaMetaPage),
    IonicPageModule.forChild(EditaMetaPage),
    IonicPageModule.forChild(AdicionaAmigoPage)
    ],
  exports: [
    AdicionaEmprestimoPage,
    AdicionaDividaPage,
    AdicionaFinancaPage,
    EditaDividaPage,
    EditaEmprestimoPage,
    EditaFinancaPage,
    BrMaskerModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FinancasPage,
    PerfilPage,
    AmigosPage,
    SobrePage,
    FinancasChartsPage,
    AtivosFinanceirosPage,
    SobrePage
    ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File, 
    HttpClient,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DividaServiceProvider,
    AuthProvider,
    OneSignal,
    AlertController,
    AcordoServiceProvider,
    NotificationProvider,
    FinancaServiceProvider,
    MetaServiceProvider,
    AmigosServiceProvider
  ]
})
export class AppModule {}
